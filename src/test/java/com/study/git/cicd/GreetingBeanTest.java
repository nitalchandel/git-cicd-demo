package com.study.git.cicd;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author chandeln
 */
public class GreetingBeanTest {

    public GreetingBeanTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testGetMessage() {
        GreetingBean instance = new GreetingBean("Hello !!!");
        String expResult = "Hello !!!";
        String result = instance.getMessage();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetMessage() {
        String expResult = "Hello !!!";
        GreetingBean instance = new GreetingBean();
        instance.setMessage("Hello !!!");
        String result = instance.getMessage();
        assertEquals(expResult, result);
    }

}
